package org.springboot.multipartfile.upload.repository;

import org.springboot.multipartfile.upload.Model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, String>{
	
	public Product findByUuid(String uuid);

}
