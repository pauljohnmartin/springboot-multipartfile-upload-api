package org.springboot.multipartfile.upload.service;

import java.io.IOException;
import java.util.List;

import org.springboot.multipartfile.upload.Model.Product;
import org.springframework.web.multipart.MultipartFile;

public interface ProductService {
	
	public Product saveProduct(MultipartFile multipartFile, Product requestProduct) throws IOException;
	public Product updateProduct(MultipartFile multipartFile, Product requestProduct) throws IOException;
	public Product findByUuid(String uuid);
	public List<Product> findAllProduct();

}
