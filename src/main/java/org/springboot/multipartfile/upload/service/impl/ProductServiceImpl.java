package org.springboot.multipartfile.upload.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springboot.multipartfile.upload.Model.Product;
import org.springboot.multipartfile.upload.repository.ProductRepository;
import org.springboot.multipartfile.upload.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	private ProductRepository productRepository;
	
	@Value("${upload-dir}")
	private String path;
	
	@Override
	@Caching(
			put = {
					@CachePut(value= "productCache", key= "#requestProduct.uuid")
			},
			evict = {
					@CacheEvict(value= "productCacheFindAll", allEntries= true)
			})
	public Product saveProduct(MultipartFile multipartFile, Product requestProduct) throws IOException {
		Product product = new Product();
		
		requestProduct.setUuid(UUID.randomUUID().toString());
		requestProduct.setProductImageFilename(uploadImage(multipartFile, requestProduct.getUuid(), requestProduct.getProductType()));
		
		product = productRepository.save(requestProduct);
		
		return product;
	}

	
	@Caching(
			put = {
					@CachePut(value= "productCache", key= "#requestProduct.uuid")
			},
			evict = {
					@CacheEvict(value= "productCacheFindAll", allEntries= true)
			})
	@Override
	public Product updateProduct(MultipartFile multipartFile, Product requestProduct) throws IOException {
		Product product = null;
		
		if(multipartFile != null ) {
			requestProduct.setProductImageFilename(uploadImage(multipartFile, requestProduct.getUuid(), requestProduct.getProductType()));
		}
		product = productRepository.save(requestProduct);
		
		return product;
	}

	@Override
	@Cacheable(value = "productCache", key = "#uuid" )
	public Product findByUuid(String uuid) {		
		return productRepository.findByUuid(uuid);
	}
	
	
	
	@Override
	@Cacheable(value = "productCacheFindAll", unless = "#result.size() == 0")
	public List<Product> findAllProduct() {
		return productRepository.findAll();
	}

	public String uploadImage(MultipartFile multipartFile, String uuid, String type) throws IOException {
		String filename = "";			
		String extension = "";
		
		
		if(multipartFile != null ) {
			
			byte[] bytes = multipartFile.getBytes();
			
			String[] splitBytes = multipartFile.getOriginalFilename().split("\\.");
			
			switch (splitBytes[1].toLowerCase()) {
			case "jpg":
				extension = "jpg";
				break;
			case "jpeg":
				extension = "jpeg";
				break;
			default:
				extension = "png";
				break;
			}					
			
			filename = uuid+"-"+type+"."+extension;	
			
			Path destination = Paths.get(path + filename);
			
			Files.write(destination, bytes);	
			
		}
		
		return filename;
	}
	
	

}
