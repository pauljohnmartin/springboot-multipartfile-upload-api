package org.springboot.multipartfile.upload.resource;

import java.io.IOException;

import org.springboot.multipartfile.upload.Model.Product;
import org.springboot.multipartfile.upload.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("products")
@CrossOrigin(origins = "http://localhost:4200")
public class ProductResource {
	
	@Autowired
	private ProductService productService;
	
	@PostMapping
	public ResponseEntity<?>  saveProduct(@RequestBody @RequestParam(value = "file") MultipartFile multipartFile, Product product) throws IOException{
		return new ResponseEntity<>(productService.saveProduct(multipartFile, product), HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<?>  updateProduct(@RequestBody @RequestParam(value = "file", required = false) MultipartFile multipartFile, Product product) throws IOException{
		return new ResponseEntity<>(productService.updateProduct(multipartFile, product), HttpStatus.CREATED);
	}
	
	@GetMapping("/uuid/{uuid}")
	public ResponseEntity<Product>  findByUuid(@PathVariable String uuid) throws IOException{
		System.out.println("Find by uuid start +++++++++++++++++");
		return new ResponseEntity<>(productService.findByUuid(uuid), HttpStatus.OK);
	}
	
	@GetMapping
	public ResponseEntity<?>  findByAll() throws IOException{		
		System.out.println("Find all start +++++++++++++++++");
		return new ResponseEntity<>(productService.findAllProduct(), HttpStatus.OK);
	}
	
	
	
	

}
